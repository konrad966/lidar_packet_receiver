
#include "parser.h"

static const float pandarGeneral_elev_angle_map[] = {
    14.882f, 11.032f, 8.059f, 5.057f, 3.04f, 2.028f, 1.86f, 1.688f, \
    1.522f, 1.351f, 1.184f, 1.013f, 0.846f, 0.675f, 0.508f, 0.337f, \
    0.169f, 0.0f, -0.169f, -0.337f, -0.508f, -0.675f, -0.845f, -1.013f, \
    -1.184f, -1.351f, -1.522f, -1.688f, -1.86f, -2.028f, -2.198f, -2.365f, \
    -2.536f, -2.7f, -2.873f, -3.04f, -3.21f, -3.375f, -3.548f, -3.712f, \
    -3.884f, -4.05f, -4.221f, -4.385f, -4.558f, -4.72f, -4.892f, -5.057f, \
    -5.229f, -5.391f, -5.565f, -5.726f, -5.898f, -6.061f, -7.063f, -8.059f, \
    -9.06f, -9.885f, -11.032f, -12.006f, -12.974f, -13.93f, -18.889f, -24.897f
};

static const float pandarGeneral_horizatal_azimuth_offset_map[] = {
    -1.042f, -1.042f, -1.042f, -1.042f,  -1.042f, -1.042f, 1.042f, 3.125f, \
    5.208f, -5.208f, -3.125f, -1.042f, 1.042f, 3.125f, 5.208f, -5.208f, \
    -3.125f, -1.042f, 1.042f, 3.125f, 5.208f, -5.208f, -3.125f, -1.042f, \
    1.042f, 3.125f, 5.208f, -5.208f, -3.125f, -1.042f, 1.042f, 3.125f, \
    5.208f, -5.208f, -3.125f, -1.042f, 1.042f, 3.125f, 5.208f, -5.208f, \
    -3.125f, -1.042f, 1.042f, 3.125f, 5.208f, -5.208f, -3.125f, -1.042f, \
    1.042f, 3.125f, 5.208f, -5.208f, -3.125f, -1.042f, -1.042f, -1.042f, \
    -1.042f, -1.042f, -1.042f, -1.042f, -1.042f, -1.042f, -1.042f, -1.042f
};

static bool packet_timeout_flag = false;

bool enable_lidar_process_thr_;
bool enable_lidar_recv_thr_;
std::thread* lidar_process_thr_ = NULL;
std::thread* lidar_recv_thr_    = NULL;
float General_elev_angle_map_[HS_LIDAR_L64_UNIT_NUM];
float General_horizontal_azimuth_offset_map_[HS_LIDAR_L64_UNIT_NUM];

std::mutex lidar_lock_;
uint16_t last_azimuth_;
int start_angle_;
std::list<PandarPacket> lidar_packets_;

// Point Cloud range
// the same values as in PP original article
const float x_min = 0;
const float x_max = 70.4;
const float y_min = -40;
const float y_max = 40;
const float z_min = -3;
const float z_max = 1;


static void initialize_arrays();

int ParseL64Data(HS_LIDAR_L64_Packet *packet, const uint8_t *recvbuf, const int len) {
    if (len != HS_LIDAR_L64_6PACKET_SIZE &&
        len != HS_LIDAR_L64_7PACKET_SIZE &&
        len != HS_LIDAR_L64_6PACKET_WITHOUT_UDPSEQ_SIZE &&
        len != HS_LIDAR_L64_7PACKET_WITHOUT_UDPSEQ_SIZE &&
        len != 1270) {
      std::cout << "packet size mismatch PandarGeneral_Internal " << len << "," << \
          len << std::endl;
      return -1;
    }

    int index = 0;
    int block = 0;
    //Parse 8 Bytes Header
    packet->header.sob = (recvbuf[index] & 0xff) << 8| ((recvbuf[index+1] & 0xff));
    packet->header.chLaserNumber = recvbuf[index+2] & 0xff;
    packet->header.chBlockNumber = recvbuf[index+3] & 0xff;
    packet->header.chReturnType = recvbuf[index+4] & 0xff;
    packet->header.chDisUnit = recvbuf[index+5] & 0xff;
    index += HS_LIDAR_L64_HEAD_SIZE;

    if (packet->header.sob != 0xEEFF) {
      printf("Error Start of Packet!\n");
      return -1;
    }

    for(block = 0; block < packet->header.chBlockNumber; block++) {
      packet->blocks[block].azimuth = (recvbuf[index] & 0xff) | \
          ((recvbuf[index + 1] & 0xff) << 8);
      index += HS_LIDAR_L64_BLOCK_HEADER_AZIMUTH;

      int unit;

      for(unit = 0; unit < packet->header.chLaserNumber; unit++) {
        unsigned int unRange = (recvbuf[index]& 0xff) | ((recvbuf[index + 1]& 0xff) << 8);

        packet->blocks[block].units[unit].distance = \
            (static_cast<double>(unRange * packet->header.chDisUnit)) / (double)1000;
        packet->blocks[block].units[unit].intensity = (recvbuf[index+2]& 0xff);
        index += HS_LIDAR_L64_UNIT_SIZE;
      }
    }

    index += HS_LIDAR_L64_RESERVED_SIZE; // skip reserved bytes
    index += HS_LIDAR_L64_ENGINE_VELOCITY;

    packet->timestamp = (recvbuf[index] & 0xff)| (recvbuf[index+1] & 0xff) << 8 | \
        ((recvbuf[index+2] & 0xff) << 16) | ((recvbuf[index+3] & 0xff) << 24);
      // printf("timestamp %u \n", packet->timestamp);
    index += HS_LIDAR_L64_TIMESTAMP_SIZE;

    packet->echo = recvbuf[index]& 0xff;

    index += HS_LIDAR_L64_ECHO_SIZE;
    index += HS_LIDAR_L64_FACTORY_SIZE;

    packet->addtime[0] = recvbuf[index]& 0xff;
    packet->addtime[1] = recvbuf[index + 1]& 0xff;
    packet->addtime[2] = recvbuf[index + 2]& 0xff;
    packet->addtime[3] = recvbuf[index + 3]& 0xff;
    packet->addtime[4] = recvbuf[index + 4]& 0xff;
    packet->addtime[5] = recvbuf[index + 5]& 0xff;

    index += HS_LIDAR_TIME_SIZE;

    return 0;
}

double degreeToRadian(double degree) { return (degree*M_PI) / 180;}

bool point_in_range(PointXYZI point) {
  if (point.x <= x_max && point.x >= x_min && point.y <= y_max && point.y >= y_min && point.z <= z_max && point.z >= z_min)
    return true;
  else
    return false;
}

void generate_voxels(PointXYZI point, std::shared_ptr<voxels_t> voxels) {
  int row = (point.x - x_min)/voxel_size;     // in x dimension
  int col = (point.y - y_min)/voxel_size;     // in y dimension
  auto current_tuple = std::make_tuple(row,col);
  bool voxel_exist = voxels->find(current_tuple) != voxels->end();
  int random_idx;
  
  if (voxel_exist || (!voxel_exist && (voxels->size() < P)) ) {
    points_in_voxel *point_voxel_ptr;
    point_voxel_ptr = &((*voxels)[current_tuple]);

    if(!voxel_exist) {
       *point_voxel_ptr = {0};
    }

    if (point_voxel_ptr->n < N) {
      point_voxel_ptr->points[point_voxel_ptr->n] = point;
      point_voxel_ptr->n++;
    }
    else {
      random_idx = std::rand() % (N+1);
      if (random_idx != N) {
        point_voxel_ptr->points[random_idx] = point;
      }
    }
  }
}

void CalcL64PointXYZIT(HS_LIDAR_L64_Packet *pkt, int blockid, char chLaserNumber, std::shared_ptr<PointCloud> pc, std::shared_ptr<voxels_t> voxels) {
    HS_LIDAR_L64_Block *block = &pkt->blocks[blockid];

    for (int i = 0; i < chLaserNumber; ++i) {
        /* for all the units in a block */
        HS_LIDAR_L64_Unit &unit = block->units[i];
        PointXYZI point;

        /* skip wrong points */
        if (unit.distance <= 0.1 || unit.distance > 200.0) {
        continue;
        }

        double xyDistance = \
            unit.distance * cos(degreeToRadian(General_elev_angle_map_[i]));
        point.x = static_cast<float>(
            xyDistance *
            sin(degreeToRadian(General_horizontal_azimuth_offset_map_[i] +
                                (static_cast<double>(block->azimuth)) / 100.0)));
        point.y = static_cast<float>(
            xyDistance *
            cos(degreeToRadian(General_horizontal_azimuth_offset_map_[i] +
                                (static_cast<double>(block->azimuth)) / 100.0)));
        point.z = static_cast<float>(unit.distance *
                                    sin(degreeToRadian(General_elev_angle_map_[i])));

        point.intensity = unit.intensity;
        if (point_in_range(point)) {
          pc->push_back(point);
          generate_voxels(point, voxels);
        }
    }
}

void process_point_cloud(std::shared_ptr<PointCloud> pc, std::shared_ptr<voxels_t> voxels) {
  static int file_counter = 0;
  PointXYZI point;
  std::ofstream outfile_pc;
  char filename_pc[25];
  sprintf(filename_pc, "chmura_%02d.bin", file_counter);
  outfile_pc.open(filename_pc, std::ios::binary | std::ios::out);
  
  while(pc->size() > 0) {
    point = pc->back();
    pc->pop_back();
    outfile_pc.write(reinterpret_cast<const char*>(&point.x), sizeof(float));
    outfile_pc.write(reinterpret_cast<const char*>(&point.y), sizeof(float));
    outfile_pc.write(reinterpret_cast<const char*>(&point.z), sizeof(float));
    outfile_pc.write(reinterpret_cast<const char*>(&point.intensity), sizeof(float));
  }
  outfile_pc.close();


  std::ofstream outfile_single_voxel;
  char filename_voxel_single[25];
  sprintf(filename_voxel_single, "single_%02d.bin", file_counter);
  outfile_single_voxel.open(filename_voxel_single, std::ios::binary | std::ios::out);

  int current_voxel_num = 0;
  std::ofstream outfile_voxel;
  char filename_voxel[25];
  sprintf(filename_voxel, "voxels_%02d.bin", file_counter);
  outfile_voxel.open(filename_voxel, std::ios::binary | std::ios::out);
  for (auto it = voxels->begin(); it != voxels->end(); it++) {
    current_voxel_num++;
    points_in_voxel& my_voxel = it->second;
    for(int i = 0; i < N; i++) {
      point = my_voxel.points[i];
      outfile_voxel.write(reinterpret_cast<const char*>(&point.x), sizeof(float));
      outfile_voxel.write(reinterpret_cast<const char*>(&point.y), sizeof(float));
      outfile_voxel.write(reinterpret_cast<const char*>(&point.z), sizeof(float));
      outfile_voxel.write(reinterpret_cast<const char*>(&point.intensity), sizeof(float));

      if(current_voxel_num == 100) {
        outfile_single_voxel.write(reinterpret_cast<const char*>(&point.x), sizeof(float));
        outfile_single_voxel.write(reinterpret_cast<const char*>(&point.y), sizeof(float));
        outfile_single_voxel.write(reinterpret_cast<const char*>(&point.z), sizeof(float));
        outfile_single_voxel.write(reinterpret_cast<const char*>(&point.intensity), sizeof(float));
      }
    }
  }

  outfile_voxel.close();
  outfile_single_voxel.close();
  file_counter++;
}

void ProcessLidarPacket() {
    constexpr double packet_timeout = 5; //sec

    int ret = 0;
    bool first_packet = true;

    std::shared_ptr<PointCloud> pc;
    pc.reset(new PointCloud());

    std::shared_ptr<voxels_t> voxels;
    voxels.reset(new voxels_t);

    auto time_start = std::chrono::system_clock::now();
    auto time_end   = std::chrono::system_clock::now();

    while (enable_lidar_process_thr_) {
        PandarPacket packet;
        bool valid_packet = false;
        lidar_lock_.lock();
        if(lidar_packets_.size() > 0) {
          packet = lidar_packets_.front();
          lidar_packets_.pop_front();
          valid_packet = true;
          time_start = std::chrono::system_clock::now();
        }
        lidar_lock_.unlock();

        if(!first_packet) {
          time_end = std::chrono::system_clock::now();
          std::chrono::duration<double> elapsed_seconds = time_end - time_start;
          if(elapsed_seconds.count() > packet_timeout) {
            packet_timeout_flag = true;
          }
        }

        if(!valid_packet){
          std::this_thread::sleep_for(std::chrono::milliseconds(10));
          continue;
        }


        HS_LIDAR_L64_Packet pkt;
        ret = ParseL64Data(&pkt, packet.data, packet.size);

        if (ret != 0)
            continue;

        for (int i = 0; i < pkt.header.chBlockNumber; ++i) {
            int azimuthGap = 0; /* To do */
            if(!first_packet) {
              if(last_azimuth_ > pkt.blocks[i].azimuth)
                  azimuthGap = static_cast<int>(pkt.blocks[i].azimuth) + (36000 - static_cast<int>(last_azimuth_));
              else
                  azimuthGap = static_cast<int>(pkt.blocks[i].azimuth) - static_cast<int>(last_azimuth_);
              
              if (last_azimuth_ != pkt.blocks[i].azimuth && azimuthGap < 600 /* 6 degree*/) {
              /* for all the blocks */
                  if ((last_azimuth_ > pkt.blocks[i].azimuth && start_angle_ <= pkt.blocks[i].azimuth) ||
                          (last_azimuth_ < start_angle_ && start_angle_ <= pkt.blocks[i].azimuth)) {
                      if (pc->size() > 0) {
                          process_point_cloud(pc, voxels);
                          std::cout << "We've got the whole pc!" << std::endl;
                          pc.reset(new PointCloud());
                          voxels.reset(new voxels_t);
                      }
                  }
              } 
              else {
                  //printf("last_azimuth_:%d pkt.blocks[i].azimuth:%d  *******azimuthGap:%d\n", last_azimuth_, pkt.blocks[i].azimuth, azimuthGap);
              }
            }
            else {
              start_angle_ = pkt.blocks[i].azimuth;
              first_packet = false;
            }
            CalcL64PointXYZIT(&pkt, i, pkt.header.chLaserNumber, pc, voxels);
            last_azimuth_ = pkt.blocks[i].azimuth;
        }
    }
}

void DummyRecvTask() {
  std::string input_filename = "hesai_packets.bin";
  constexpr int num_packets = 3000;
  constexpr int packet_len  = 1194;
  constexpr int timeout     = 1; //msec
  
  PandarPacket packet;
  packet.size = packet_len;

  std::ifstream infile;
  infile.open(input_filename, std::ios::binary | std::ios::in);
  for(int i = 0; i < num_packets; i++) {
    infile.read((char*)(packet.data), packet_len);
    lidar_lock_.lock();
    lidar_packets_.push_back(packet);
    lidar_lock_.unlock();
    std::this_thread::sleep_for(std::chrono::milliseconds(timeout));
  }
  infile.close();
  while(enable_lidar_recv_thr_) {
    std::chrono::milliseconds(timeout);
  }
}

int RecvTask() {
  constexpr int packet_len  = 1194;
  int sockfd;
	struct addrinfo hints, *servinfo, *p;
	int rv;
	int numbytes;
	struct sockaddr_storage their_addr;
	char buf[ETHERNET_MTU];
	socklen_t addr_len;
  struct timeval tv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, MYPORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("listener: socket");
			continue;
		}

    tv.tv_sec  = 1;
    tv.tv_usec = 0;
    if (setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(tv)) < 0) {
        perror("Error");
    }

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("listener: bind");
			continue;
		}    
		break;
	}

	if (p == NULL) {
		fprintf(stderr, "listener: failed to bind socket\n");
		return 2;
	}

	freeaddrinfo(servinfo);
	printf("listener: waiting to recvfrom...\n");

  PandarPacket packet;
  packet.size = packet_len;

  while(enable_lidar_recv_thr_) {
    addr_len = sizeof their_addr;
    if ((numbytes = recvfrom(sockfd, buf, ETHERNET_MTU-1 , 0,
      (struct sockaddr *)&their_addr, &addr_len)) == -1) {
      //perror("recvfrom");
      //exit(1);
    }
    else {
      if(numbytes == packet_len) {
        memcpy(packet.data, buf, numbytes);
        lidar_lock_.lock();
        lidar_packets_.push_back(packet);
        lidar_lock_.unlock();
      }
    }
  }

	close(sockfd);
	return 0;
}

static void initialize_arrays() {
  for (int i = 0; i < HS_LIDAR_L64_UNIT_NUM; i++) {
    // for all the laser offset 
    General_elev_angle_map_[i] = pandarGeneral_elev_angle_map[i];
    General_horizontal_azimuth_offset_map_[i] = \
        pandarGeneral_horizatal_azimuth_offset_map[i];
  }
}

bool end_event() {
  return packet_timeout_flag;
}

void Start() {
  initialize_arrays();
  Stop();
  enable_lidar_recv_thr_ = true;
  enable_lidar_process_thr_ = true;
  lidar_recv_thr_    = new std::thread(RecvTask);
  lidar_process_thr_ = new std::thread(ProcessLidarPacket);
}

void Stop() {
  enable_lidar_recv_thr_ = false;
  enable_lidar_process_thr_ = false;

  if (lidar_process_thr_) {
    lidar_process_thr_->join();
    delete lidar_process_thr_;
    lidar_process_thr_ = NULL;
  }

  if (lidar_recv_thr_) {
    lidar_recv_thr_->join();
    delete lidar_recv_thr_;
    lidar_recv_thr_ = NULL;
  }
  return;
}
