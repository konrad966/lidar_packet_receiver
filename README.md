## Description

This is a repository with LiDAR UDP receiver application.
The code is based on <a href="https://github.com/HesaiTechnology/HesaiLidar_PandarGeneral_ros" target="_blank">this</a> Hesai repository.
It is adapted to Pandar64 LiDAR.
It has been tested on Ubuntu 18.04 LTS and Xilinx <a href="https://www.xilinx.com/products/design-tools/embedded-software/petalinux-sdk.html" target="_blank">Petalinux</a>

You can find test pcap files on Hesai official <a href="https://www.hesaitech.com/en/download" target="_blank">website</a>.

## Application principle of operation

The C++ application receives LiDAR data by UDP.
Then it extracts points from received packets and converts them to cartesian coordinates with meter as a distance unit.
It also discards points outside the given point cloud range.
What is more it collects points into voxels of predefined size.
The point discard and voxelization is based on PointPillars data preprocessing (see <a href="https://arxiv.org/abs/1812.05784" target="_blank">paper</a>).
When it gets the full 360 degree point cloud, it saves it to the file.


## Getting started

The C++ ```lidar_parser``` application (built with ```make```) is meant to run on an embedded platform, but you may test it on your PC as well.
The python ```send_lidar_pcap.py``` app is a LiDAR emulator and is meant to run on a PC computer.

1. Clone this repo.

2. Go to the repo directory and compile C++ application by typing ```make```.
Compilation for other platforms than host PC may look differently, but the ```Makefile``` in this repo should be a good template.

3. Set IP address in ```send_lidar_pcap.py``` to address of a platform with C++ app (if you want to test the app locally, you may use ```127.0.0.1```).

4. Set ```pcap_filename``` in ```send_lidar_pcap.py``` accordingly to your test pcap filename.

5. First run the compiled C++ application and then the ```send_lidar_pcap.py```. C++ application should continously save received point clouds (it saves a point clouds after it receives full 360 degree range).
