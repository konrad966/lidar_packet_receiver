
#ifndef __PARSER_H__
#define __PARSER_H__

#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <cerrno>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <map>
#include <array>
#include <tuple>
#include <string>
#include <thread>
#include <mutex>
#include <memory>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <ctime>

#ifndef M_PI
#define M_PI 3.141592655358979323846
#endif

/* ---------------------------- Defines ------------------------------- */
#define MYPORT "4950" // the port users will be connecting to

#define ETHERNET_MTU 1500

#define HS_LIDAR_TIME_SIZE (6)
// Each Packet have 8 byte
#define HS_LIDAR_L64_HEAD_SIZE (8)
// Block number 6 or 7
#define HS_LIDAR_L64_BLOCK_NUMBER_6 (6)
#define HS_LIDAR_L64_BLOCK_NUMBER_7 (7)

// each block first 2 byte  is azimuth
#define HS_LIDAR_L64_BLOCK_HEADER_AZIMUTH (2)
// each block have  64 Unit
#define HS_LIDAR_L64_UNIT_NUM (64)
// each Unit have 3 byte :2 bytes(distance) + 1 byte(intensity)
#define HS_LIDAR_L64_UNIT_SIZE (3)
// total block size 194
#define HS_LIDAR_L64_BLOCK_SIZE (HS_LIDAR_L64_UNIT_SIZE *    \
                                     HS_LIDAR_L64_UNIT_NUM + \
                                 HS_LIDAR_L64_BLOCK_HEADER_AZIMUTH)

// Block tail = timestamp ( 4 bytes ) + factory num (2 bytes)
#define HS_LIDAR_L64_TIMESTAMP_SIZE (4)
#define HS_LIDAR_L64_ECHO_SIZE (1)
#define HS_LIDAR_L64_FACTORY_SIZE (1)
#define HS_LIDAR_L64_RESERVED_SIZE (8)
#define HS_LIDAR_L64_ENGINE_VELOCITY (2)

// packet body size two type
#define HS_LIDAR_L64_6_BLOCK_PACKET_BODY_SIZE (HS_LIDAR_L64_BLOCK_SIZE * \
                                               HS_LIDAR_L64_BLOCK_NUMBER_6)
#define HS_LIDAR_L64_7_BLOCK_PACKET_BODY_SIZE (HS_LIDAR_L64_BLOCK_SIZE * \
                                               HS_LIDAR_L64_BLOCK_NUMBER_7)

// packet tail size
#define HS_LIDAR_L64_PACKET_TAIL_SIZE (26)
#define HS_LIDAR_L64_PACKET_TAIL_WITHOUT_UDPSEQ_SIZE (22)

//total packet size two type,length: 1198 and 1392
#define HS_LIDAR_L64_6PACKET_SIZE (HS_LIDAR_L64_HEAD_SIZE + \
                                   HS_LIDAR_L64_6_BLOCK_PACKET_BODY_SIZE + HS_LIDAR_L64_PACKET_TAIL_SIZE)
#define HS_LIDAR_L64_7PACKET_SIZE (HS_LIDAR_L64_HEAD_SIZE + \
                                   HS_LIDAR_L64_7_BLOCK_PACKET_BODY_SIZE + HS_LIDAR_L64_PACKET_TAIL_SIZE)

#define HS_LIDAR_L64_6PACKET_WITHOUT_UDPSEQ_SIZE (HS_LIDAR_L64_HEAD_SIZE + \
                                                  HS_LIDAR_L64_6_BLOCK_PACKET_BODY_SIZE + HS_LIDAR_L64_PACKET_TAIL_WITHOUT_UDPSEQ_SIZE)
#define HS_LIDAR_L64_7PACKET_WITHOUT_UDPSEQ_SIZE (HS_LIDAR_L64_HEAD_SIZE + \
                                                  HS_LIDAR_L64_7_BLOCK_PACKET_BODY_SIZE + HS_LIDAR_L64_PACKET_TAIL_WITHOUT_UDPSEQ_SIZE)

// Point Cloud range
// the same values as in PP original article
extern const float x_min;
extern const float x_max;
extern const float y_min;
extern const float y_max;
extern const float z_min;
extern const float z_max;

constexpr float voxel_size = 0.16;  // Voxels size
constexpr int P            = 12000; // Voxels number
constexpr int N            = 100;   // Points in voxel

/* ---------------------------- Typedefs ------------------------------- */
typedef struct PandarPacket_s {
  double stamp;
  uint8_t data[ETHERNET_MTU];
  uint32_t size;
} PandarPacket;

typedef struct HS_LIDAR_L64_Unit_s {
  double distance;
  unsigned short intensity;
} HS_LIDAR_L64_Unit;

typedef struct HS_LIDAR_L64_Block_s {
  unsigned short azimuth; // packet angle  ,Azimuth = RealAzimuth * 100
  HS_LIDAR_L64_Unit units[HS_LIDAR_L64_UNIT_NUM];
} HS_LIDAR_L64_Block;

typedef struct HS_LIDAR_L64_Header_s {
  unsigned short sob; // 0xFFEE 2bytes
  char chLaserNumber; // laser number 1byte
  char chBlockNumber; //block number 1byte
  char chReturnType;  // return mode 1 byte  when dual return 0-Single Return
                      // 1-The first block is the 1 st return.
                      // 2-The first block is the 2 nd return
  char chDisUnit;     // Distance unit, 6mm/5mm/4mm
public:
  HS_LIDAR_L64_Header_s(){
    sob = 0;
    chLaserNumber = 0;
    chBlockNumber = 0;
    chReturnType = 0;
    chDisUnit = 0;
  }
} HS_LIDAR_L64_Header;

typedef struct HS_LIDAR_L64_Packet_s{
  HS_LIDAR_L64_Header header;
  HS_LIDAR_L64_Block blocks[HS_LIDAR_L64_BLOCK_NUMBER_7];
  unsigned int timestamp; // ms
  unsigned int echo;
  unsigned char addtime[6];
} HS_LIDAR_L64_Packet;

/* ---------------------------- Point   --------------------------------- */
struct PointXYZI{
  float x;
  float y;
  float z;
  float intensity;
};
typedef std::vector<PointXYZI> PointCloud;

/* ---------------------------- Voxels  --------------------------------- */
struct points_in_voxel {
  int n;
  std::array<PointXYZI, N> points;
};
typedef std::map<std::tuple<int, int>, points_in_voxel> voxels_t;



/* ---------------------------- Globals --------------------------------- */
extern bool enable_lidar_process_thr_;
extern bool enable_lidar_recv_thr_;
extern std::thread* lidar_process_thr_;
extern std::thread* lidar_recv_thr_;

extern std::mutex lidar_lock_;
extern uint16_t last_azimuth_;
extern int start_angle_;
extern std::list<PandarPacket> lidar_packets_;

extern float General_elev_angle_map_[HS_LIDAR_L64_UNIT_NUM];
extern float General_horizontal_azimuth_offset_map_[HS_LIDAR_L64_UNIT_NUM];

/* ---------------------------- Functions ------------------------------- */
int ParseL64Data(HS_LIDAR_L64_Packet *packet, const uint8_t *recvbuf, const int len);
void CalcL64PointXYZIT(HS_LIDAR_L64_Packet *pkt, int blockid, char chLaserNumber, std::shared_ptr<PointCloud> pc);
void ProcessLidarPacket();
void process_point_cloud(std::shared_ptr<PointCloud> pc);
void Start();
void Stop();
int RecvTask();
void DummyRecvTask();
bool end_event();

#endif //__PARSER_H__
