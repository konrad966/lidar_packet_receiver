#include "parser.h"

int main() {

    srand(time(NULL));
    
    Start();

    while(!end_event()) {
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }

    Stop();

    std::cout << "App exit!" << std::endl;
    return 0;
}
