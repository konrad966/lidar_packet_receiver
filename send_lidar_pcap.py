import socket
import sys
import dpkt
import time

class send_pcap():
    def __init__(self, udp_ip, udp_port, pcap_filename, packets_num, frequency):
        self._UDP_IP = udp_ip
        self._UDP_PORT = udp_port
        self._pcap_filename = pcap_filename
        self._packets_num = packets_num
        self._frequency = frequency
        self._sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)

    def get_packets(self):
        packets = []
        try:
            with open(self._pcap_filename, "rb") as file:
                pcap_reader = dpkt.pcap.Reader(file)
                for i, packet in enumerate(pcap_reader):
                    ts, buff = packet
                    eth      = dpkt.ethernet.Ethernet(buff)
                    ip       = eth.data
                    udp      = ip.data
                    packets.append(udp.data)
                    if i+1 >= self._packets_num:
                        break
        except Exception as e:
            print("get_packets exception: {}".format(e))
        return packets

    def close_socket(self):
        self._sock.close()

    def main(self):
        packets = self.get_packets()
        for i in range (self._packets_num):
            packet = packets[i]
            self._sock.sendto(packet, (self._UDP_IP, self._UDP_PORT))
            time.sleep(1/self._frequency)
        self.close_socket()


if __name__ == "__main__":
    UDP_IP = "127.0.0.1"
    UDP_PORT = 4950
    pcap_filename = "Crowded Crossing_Pandar64.pcap"
    packets_num = 2000
    frequency = 100
    my_object = send_pcap(UDP_IP, UDP_PORT, pcap_filename, packets_num, frequency)
    my_object.main()